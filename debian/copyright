Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Paranoid
Upstream-Contact: Arthur Corliss <corliss@digitalmages.com>
Source: https://metacpan.org/release/Paranoid

Files: *
Copyright: 2005-2021, Arthur Corliss <corliss@digitalmages.com>
License: other
 This software is free software.  Similar to Perl, you can redistribute it
 and/or modify it under the terms of either:
 .
   a)     the GNU General Public License
          <https://www.gnu.org/licenses/gpl-1.0.html> as published by the
          Free Software Foundation <http://www.fsf.org/>; either version 1
          <https://www.gnu.org/licenses/gpl-1.0.html>, or any later version
          <https://www.gnu.org/licenses/license-list.html#GNUGPL>, or
   b)     the Artistic License 2.0
          <https://opensource.org/licenses/Artistic-2.0>,
 .
 subject to the following additional term:  No trademark rights to
 "Paranoid" have been or are conveyed under any of the above licenses.
 However, "Paranoid" may be used fairly to describe this unmodified
 software, in good faith, but not as a trademark.
 .
 (c) 2005 - 2021, Arthur Corliss (corliss@digitalmages.com)
 (tm) 2008 - 2021, Paranoid Inc. (www.paranoid.com)

Files: debian/*
Copyright: 2009, Ryan Niebur <ryan@debian.org>
 2010, Jonathan Yu <jawnsy@cpan.org>
 2010-2024, gregor herrmann <gregoa@debian.org>
 2016-2017, Lucas Kanashiro <kanashiro@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
